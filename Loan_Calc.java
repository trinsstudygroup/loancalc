/*
Name: Christopher Hurst
Class: CSC240
Due Date: 06/19/16
Files: Loan_Calc.java
Description: The program will prompt the user to ender the total loan amount, the
    number of months the loan will have, and the quoted APR. it will then give a 
    break down of the total of months it will to pay off the loan. 
 */

package loan_calc;

import java.util.Scanner;
import java.text.DecimalFormat;

public class Loan_Calc {
   
    public static void main(String[] args) {
    
    Scanner input = new Scanner(System.in);
    DecimalFormat DF = new DecimalFormat("0.00"); 
    
 
//The start of the repeat statement
    String redo = "";
    do{
        System.out.println("Welcome to the Loan Amortization Program of Chris Banking");
        System.out.println("This program will help you determine the payment structure");
        System.out.println("    of your loan.");
        System.out.print("Let us get started: \n \n");
// Asks the user to input their data    
        System.out.print("Enter the loan amount you are requesting: ");
        double loanAmount = input.nextDouble();    
        System.out.print("Enter the number of months for the loan: ");
        int numMonths = input.nextInt();
        System.out.print("Enter the APR you have been quoted: ");
        double apr = input.nextDouble();
// Convert interest rate into a decimal
        apr /= 100.0;
// Convert interest rate to monthly rate       
        double monthlyInterest = apr / 12.0;
// Calculate the monthly payment
        double monthlyPayment = loanAmount * monthlyInterest / (1 - 
                (1 / Math.pow(1 + monthlyInterest, numMonths)));
// Calculate the total loan with interest
        double totalPayment = monthlyPayment * numMonths;
// Calculate the interest on the first payment amount       
        double interest = monthlyInterest * loanAmount;
// Calculate the principal of the first payment        
        double principal = monthlyPayment - interest;
// Calculate the balance after the first payment is made        
        double balance = loanAmount - monthlyPayment + interest;
         
        System.out.println("The monthly payment will be: " + DF.format(monthlyPayment));    
        System.out.println("The total paid (with intrest) will be: " + DF.format(totalPayment));
        System.out.print("Your Amortization Schedule \n\n" );
        System.out.println("Payment #     Interest    Principal   Balance");
// Starts the for statement to calculate the numbers per month        
        for(int i=1; i <= numMonths; i++){
            System.out.println(i + "             " + DF.format(interest) + "    "
                    + "   " + DF.format(principal) + "      " + DF.format(balance));
// Once the statements print they are then recalculated after each payment
            loanAmount = balance;
            interest = monthlyInterest * loanAmount;
            principal = monthlyPayment - interest;
            balance = loanAmount - monthlyPayment + interest;
        }
// Finishes the repeat statment
        System.out.print("Would you like to calculate another loan (Y/N): ");
        redo = input.next();
        redo = redo.toUpperCase();
    }while(redo.equals("Y"));
    
        System.out.println("Thank you for using the Loan Amortization Program of"
                + " Chris Banking. \nGoodbye.");           
    }
    
}
